
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Midterm1 {

    static int[] reverse(int[] input) {
        int first = 0;
        int last = input.length-1;
        while(first < last){
            int temp = input[first];
            input[first] = input[last];
            input[last] = temp;
            first = first + 1;
            last = last -1 ;
        }

        return input;
    }

    public static void main(String[] args) throws IOException {
        try
        {
			for(int n=1;n<=10;n++) {
				File file = new File(n+".in");
	        	InputStream inputStream = new FileInputStream(file);
	        	
	            Scanner scanner = new Scanner(inputStream);
	            String data =  scanner.nextLine();         
	            data = data.substring(1,data.length()-1);       
	            String[] A_String = data.split(",");   
	            int[] A = new int[A_String.length];
	            for(int i = 0; i < A_String.length; i++){
	                A[i] = Integer. parseInt(A_String[i]);
	            }

	            
	            reverse(A);
	    		System.out.println("output = "+ Arrays.toString(A));
	            
	            PrintWriter writer;
	    		try {
	    			writer = new PrintWriter(n+".out", "UTF-8");
	    			writer.print(Arrays.toString(A));
	    			writer.close();
	    		} catch (FileNotFoundException e) {
	    			e.printStackTrace();
	    		} catch (UnsupportedEncodingException e) {
	    			e.printStackTrace();
	    		}
			} 
        } catch (Exception e)
        {
            e.printStackTrace ();
        }
    } 

}
